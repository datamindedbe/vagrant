#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive
apt-get update -yqq
apt-get install apt-transport-https ca-certificates
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
apt-get update -yqq
apt-get purge lxc-docker
apt-cache policy docker-engine
apt-get update -yqq
apt-get install linux-image-extra-$(uname -r)
apt-get install -yqq libpq-dev
apt-get install -yqq python-psycopg2 
apt-get install -yqq apparmor docker-engine=1.10.3-0~trusty netcat curl python-pip python-dev libmysqlclient-dev libkrb5-dev libsasl2-dev libssl-dev libffi-dev build-essential python-mysqldb
pip install ansible==2.0.1.0 
pip install boto
pip install boto3
pip install MySQL-python
pip install awscli
pip install -U docker-compose==1.6.2
chmod +x /usr/local/bin/docker-compose
